from util.deepface_connector import Connector
from util.deepface_connector import Translater  # 先引入翻译器

if __name__ == '__main__':
    conn = Connector()
    translater = Translater()
    attribute = conn.facial_attribute_analysis(r"E:\faceDB\mask.jpg", actions=["race", "emotion"])

    race = attribute.get_race()
    emotion = attribute.get_emotion()

    print("人种:", translater.to_chinese(race))
    print("情绪:", translater.to_chinese(emotion))
