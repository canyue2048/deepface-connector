# deepface connector



<p align="center"><img src="./image/deepface-connector-icon.png" width="200" height="200"></p>

deepface转接件，封装deepface部分方法，方便开发者调用

适配deepface版本: 0.0.85，理论上方法调用方式不变，转接件仍旧兼容

[deepface 项目地址](https://github.com/serengil/deepfac)

# 开始之前

在使用转接件之前，请确保当前开发环境中已经安装deepface

你可以使用pip安装

```
pip install deepface
```

如果你本地安装的是Conda

``` 
conda install -c conda-forge deepface
```

也可以参考原项目README文件中的方法，[传送门](https://github.com/serengil/deepface#installation--)

# 如何使用

首先，你需要将项目中的`deepface_connector.py`文件放置在你的项目中。

例如我将文件放置于项目目录下util文件夹下

然后通过以下代码导入deepface connector

```python
from util.deepface_connector import Connector   #这是连接件
from util.deepface_connector import Translater  #这是翻译器
```

## 转接件

转接件对deepface中`analyze()`、`find()`、`verify()`三个方法进行封装，并简化其使用方法

初始化转接件的方法很简单

```py
conn = Connector()
```

### 分析人脸属性

转接件封装了analyze()方法，并对其使用进行了简化

可以参考以下代码，对图片中的人脸数据进行分析

```python
from util.deepface_connector import Connector

if __name__ == '__main__':
    conn = Connector()
    attribute = conn.facial_attribute_analysis(r"E:\faceDB\mask.jpg")
```

> 注意：与deepface的analyze()方法不同，此时将返回一个Attribute的实例化对象，通过对象中的方法可以得到相应的预测数据

### 获得预测数据

Attribute类中定义了两个方法用于获取情绪的预测数据，你可以参考下面的代码

```python
from util.deepface_connector import Connector

if __name__ == '__main__':
    conn = Connector()
    attribute = conn.facial_attribute_analysis(r"E:\faceDB\mask.jpg")
    print(attribute.get_emotion_score())  # 这是各情绪数据的分值
    print(attribute.get_emotion())  # 这是预测的最有可能的情绪
```

```
{'angry': 2.1139755845069885, 'disgust': 4.4924081815667094e-10, 'fear': 5.834420397877693, 'happy': 14.551383256912231, 'sad': 17.623743414878845, 'surprise': 0.05149855278432369, 'neutral': 59.824979305267334}
neutral
```

> 对，现在只要一个路径或者可以是BGR格式的numpy数组、base64编码的图像，就可以直接进行人脸属性分析
>
> 同样，年龄、性别、人种也类似

注意，为加速模型运算，`人种模型默认不加载`，如需判断人种，请参考以下代码，在调用`facial_attribute_analysis`方法时通过`actions`参数指定，你可以参考以下代码

```python
from util.deepface_connector import Connector

if __name__ == '__main__':
    conn = Connector()
    attribute = conn.facial_attribute_analysis(r"E:\faceDB\mask.jpg", actions=["race"])
    print(attribute.get_race_score())  # 这是各人种数据的分值
    print(attribute.get_race())  # 这是预测的最有可能的人种
```

```
{'asian': 1.4730504713952541, 'indian': 0.8404270745813847, 'black': 0.0961722747888416, 'white': 81.25157952308655, 'middle eastern': 8.549781888723373, 'latino hispanic': 7.788995653390884}
white
```

可以发现，和deepface自带的属性是一样的，这么做也是为了减少不必要的学习成本

与此同时，我们在封装方法时也对函数注释进行了一点翻译，你可以通过支持的IDE来查看方法中各参数的说明

![](image/image-20240307165625994.png)

## 在数据库中匹配人脸

你可以使用`best_match_by_db`在本地人脸数据库中搜寻匹配的人脸信息

如下，img_path是待匹配的图片，它可以使路径或者可以是BGR格式的numpy数组、base64编码的图像

db_path则是本地人脸数据库的路径

```python
from util.deepface_connector import Connector

if __name__ == '__main__':
    conn = Connector()
    print(conn.best_match_by_db(img_path=r"E:\test.jpg", db_path=r"E:\faceDB"))
```

```
mask
```

转接件默认你本地数据库是以图片名来标识用户

就像这样

```
db
|--user1.jpg
|--user2.jpg
```

若你的数据库为了识别的准确性，是用目录名称表示用户，就像这样

```
db
|--user1
|	|-----neutral.jpg
|	|-----glasses.jpg
|--user2
|	|-----neutral.jpg
|	|-----glasses.jpg
```

那你可以通过`db_key_is_filename`属性，将其设置为False，这样转接件在解析用户名是就不会使用文件名作为用户名输出，就像这样:

```python
conn.best_match_by_db(img_path=r"E:\test.jpg", db_path=r"E:\faceDB",db_key_is_filename=False)
```



若你在使用文件名表示用户时，若向返回完整的文件名，你可以通过`file_suffix`属性，将其设置为True就可以返回完整的文件名

> 注意当db_key_is_filename=False时，这个参数无意义

```python
conn.best_match_by_db(img_path=r"E:\test.jpg", db_path=r"E:\faceDB",file_suffix=True)
```

```
mask.jpg
```

### 验证两张照片是不是同一个人

你可以使用`verify_face`方法判断传入的照片是不是同一个人，方法将直接返回布尔值，就像这样:

```python
from util.deepface_connector import Connector

if __name__ == '__main__':
    conn = Connector()
    print(conn.verify_face(r"E:\test.jpg", r"E:\faceDB\mask.jpg"))
    print(conn.verify_face(r"E:\test.jpg", r"E:\faceDB\xiaohong.jpg"))
```

```
True
False
```

### 翻译英文输出

转接件提供了一个类专门用于对deepface中部分英文输出进行翻译，使用方法如下

```python
from util.deepface_connector import Connector
from util.deepface_connector import Translater  # 先引入翻译器

if __name__ == '__main__':
    conn = Connector()
    translater = Translater()
    attribute = conn.facial_attribute_analysis(r"E:\faceDB\mask.jpg", actions=["race", "emotion"])

    race = attribute.get_race()
    emotion = attribute.get_emotion()

    print("人种:", translater.to_chinese(race))
    print("情绪:", translater.to_chinese(emotion))

```

```
人种: 白种人
情绪: 中立
```

